import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class CardGameTests {

    @Test
    void addCard() {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('S', 12));
        handOfCards.addCard(new PlayingCard('H', 3));
        handOfCards.addCard(new PlayingCard('H', 8));
        handOfCards.addCard(new PlayingCard('S', 12));
        assertEquals(3, handOfCards.getHandOfCards().size());
    }

    @Test
    void hasFlush() {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('S', 1));
        handOfCards.addCard(new PlayingCard('S', 2));
        handOfCards.addCard(new PlayingCard('S', 3));
        handOfCards.addCard(new PlayingCard('S', 4));
        handOfCards.addCard(new PlayingCard('S', 5));
        assertTrue(handOfCards.hasFlush());
    }

    @Test
    void NoFlush() {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('S', 12));
        handOfCards.addCard(new PlayingCard('H', 12));
        handOfCards.addCard(new PlayingCard('H', 13));
        handOfCards.addCard(new PlayingCard('H', 2));
        handOfCards.addCard(new PlayingCard('H', 4));
        assertFalse(handOfCards.hasFlush());
    }

    @Test
    void hasQueenOfSpades() {
        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(new PlayingCard('S', 12));
        assertTrue(handOfCards.containsQofS());
    }
}
