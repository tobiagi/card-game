import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    ArrayList<PlayingCard> cardDeck = new ArrayList<>();

    /**
     * Creates a new deck of playingcards
     */
    public DeckOfCards() {
        for (char suit: suit) {
            for (int face = 1; face <= 13; face++) {
                cardDeck.add(new PlayingCard(suit, face));
            }
        }
    }

    /**
     * Method deals hand
     * @param n amount of cards to be added to hand
     * @return the hand of cards
     */
    public HandOfCards dealHand(int n) {
        HandOfCards handOfCards = new HandOfCards();
        Random rand = new Random();

        for (int j = 0; j <= n; j++) {
            PlayingCard card = cardDeck.get(rand.nextInt(52));
            if (handOfCards.getHandOfCards().contains(card)) {
                n++;
            } else {
                handOfCards.addCard(card);
            }
        }
        return handOfCards;
    }
}