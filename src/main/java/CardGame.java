import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class CardGame extends Application {

    HandOfCards handOfCards = new HandOfCards();
    DeckOfCards cardDeck = new DeckOfCards();

    /**
     * Setting up texts for different methods
     */
    Text queenOfSpadesT = new Text();
    Text cardsOfHeartT = new Text();
    Text sumOfFacesT = new Text();
    Text flushT = new Text();

    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("Card game");

        /**
         * Setting up and configuring stackpane
         */
        StackPane stack = new StackPane();
        stack.setStyle("-fx-border-color: black");
        stack.setMinSize(300,200);

        /**
         * Creating buttons
         */
        Button dealHand = new Button("Deal hand");
        Button checkHand = new Button("Check hand");

        /**
         * Creating display for cards
         */
        Label cardsDisplay = new Label("Cards here");

        /**
         * Setting up gridpane
         */
        GridPane grid = new GridPane();
        grid.setMinSize(700,400);
        grid.setPadding(new Insets(60,10,10,60));
        grid.setHgap(20);
        grid.setVgap(40);

        /**
         * Actions when Deal hand button is pressed
         */
        dealHand.setOnAction(e -> {
            resetHand();
            handOfCards = cardDeck.dealHand(4);
            cardsDisplay.setText(handOfCards.handOfCardsDisplay());
        });

        /**
         * Actions when Check han button is pressed
         */
        checkHand.setOnAction(e -> {
            if (handOfCards.containsQofS()) {
                queenOfSpadesT.setText("Yes");
            } else if (!handOfCards.containsQofS()) {
                queenOfSpadesT.setText("No");
            }
            if (handOfCards.hasFlush()) {
                flushT.setText("Yes");
            } else if (!handOfCards.hasFlush()) {
                flushT.setText("No");
            }
            if (!handOfCards.heartOfCards().isBlank()) {
                cardsOfHeartT.setText(handOfCards.heartOfCards());
            } else if (handOfCards.heartOfCards().isBlank()) {
                cardsOfHeartT.setText("No hearts");
            }
            sumOfFacesT.setText(handOfCards.sumFaces());
        });

        /**
         * Setting up and configuring textboxes
         */
        stack.getChildren().add(cardsDisplay);
        grid.add(stack, 0,1,5,6);
        grid.add(flushT,8,8,1,1);
        grid.add(cardsOfHeartT,4,8,1,1);
        grid.add(sumOfFacesT,2,8,1,1);
        grid.add(queenOfSpadesT,6,8,1,1);

        /**
         * Placing buttons
         */
        grid.add(checkHand,8,3, 3, 1);
        grid.add(dealHand,8,2, 3, 1);

        /**
         * Setting up and configuring labels
         */
        grid.add(new Label("Sum of the faces: "),2,7);
        GridPane.setHalignment(sumOfFacesT, HPos.CENTER);
        grid.add(new Label("Cards of Heart: "),4,7);
        GridPane.setHalignment(queenOfSpadesT, HPos.CENTER);
        grid.add(new Label("Queen of spades: "),6,7);
        grid.add(new Label("Flush: "),8,7);
        GridPane.setHalignment(flushT, HPos.CENTER);

        /**
         * Creating scene
         */
        Scene scene = new Scene(grid, 800, 500);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Method to remove all preexisting text
     */
    private void resetHand() {
        handOfCards.emptyHand();
        cardsOfHeartT.setText("");
        sumOfFacesT.setText("");
        flushT.setText("");
        queenOfSpadesT.setText("");
        cardsOfHeartT.setText("");
    }
}
