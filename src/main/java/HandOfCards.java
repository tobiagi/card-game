import java.util.ArrayList;

public class HandOfCards {
    private final ArrayList<PlayingCard> handOfCards = new ArrayList<>();
    PlayingCard queenOfSpades = new PlayingCard('S', 12);
    private String hearts = "";
    private String handString = "";

    /**
     * Method to check if the hand contains the queen of spades
     * @return true if hand contains queen of spades, false if not
     */
    public boolean containsQofS() {
        return handOfCards.contains(queenOfSpades);
    }

    /**
     * Method to check if hand has a flush (5 cards with the same suit)
     * @return true if hand has flush, false if not
     */
    public boolean hasFlush() {
        long handOfS = handOfCards.stream().filter(c -> c.getSuit() == 'S').count();
        long handOfH = handOfCards.stream().filter(c -> c.getSuit() == 'H').count();
        long handOfD = handOfCards.stream().filter(c -> c.getSuit() == 'D').count();
        long handOfC = handOfCards.stream().filter(c -> c.getSuit() == 'C').count();
        return handOfS >= 5 || handOfH >= 5 || handOfD >= 5 || handOfC >= 5;
    }

    /**
     * Method to show all cards of hearts on hand
     * @return cards of hearts
     */
    public String heartOfCards() {
        handOfCards.stream().filter(c -> c.getSuit() == 'H').forEach(h -> hearts += h.getAsString() + " ");
        return hearts;
    }

    /**
     * Method to display hand of cards as a string
     * @return hand of cards as a string
     */
    public String handOfCardsDisplay() {
        getHandOfCards().forEach(c -> handString += c.getAsString() + " ");
        return handString;
    }

    /**
     * Method to find sum of all faces
     * @return sum of faces as a string
     */
    public String sumFaces() {
        return handOfCards.stream().map(PlayingCard::getFace).reduce((a, b) -> a + b).get().toString();
    }

    /**
     * Method to get hand of cards
     * @return hand of cards
     */
    public ArrayList<PlayingCard> getHandOfCards() {
        return handOfCards;
    }

    /**
     * Method to check if hand is empty
     */
    public void emptyHand() {
        if (handOfCards.size() > 0) {
            handOfCards.clear();
        }
    }

    /**
     * Method to add a card
     * @param card to be added
     */
    public void addCard(PlayingCard card) {
        if (!handOfCards.contains(card)) {
            handOfCards.add(card);
        }
    }
}
